
<head>
<link rel="stylesheet" href="bootstrap.css">
  <script src="jquery.js"></script>
  <script src="bootstrap.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
  </style>

  <link rel="stylesheet" href="animate.css">

<style>
body {
    background-image: url("1.jpg");
    background-repeat: no-repeat;
}
table
{
	margin: auto;
}
button {
	opacity: 0.7;
	-moz-box-shadow: 4px 6px 10px 0px #000000;
	-webkit-box-shadow: 4px 6px 10px 0px #000000;
	box-shadow: 4px 6px 10px 0px #000000;
	background-color:green;
	-moz-border-radius:10px;
	-webkit-border-radius:10px;
	border-radius:10px;
	border:0px solid #18ab29;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:17px;
	font-weight:bold;
	padding:65px 39px;
	text-decoration:none;
	text-shadow:4px 4px 7px #000000;
	white-space: normal;
	height: 200px;
}

button:active {
	position:relative;
	top:1px;
}

</style>
</head>
<script>


function show_div(item)
{
	document.getElementById("test1").style.display = "none";
	document.getElementById("test2").style.display = "none";
	document.getElementById("test3").style.display = "none";
	document.getElementById("test4").style.display = "none";
	document.getElementById("test5").style.display = "none";
	document.getElementById("test10").style.display = "none";
	
	document.getElementById(item).style.display = "block";
	return false;
}
</script>
<table cellpadding="50" border=0><tr><td>
<button id="b1" class=" animated bounceInLeft" onclick="return show_div('test1')">Integrated <br>Security system</button>
</td><td><button class=" animated bounceInRight" onclick="return show_div('test2')"  style="background-color:red;">Communication<br> and<br> telecomm <br>infrastructure</button>
</td><td><button class=" animated bounceInUp" onclick="return show_div('test3')"  style="background-color:yellow;">Smart <br>InfraStructure</button>
</td></tr></table>
<div id="test1" style="display:none">
<table cellpadding="50" border=0><tr><td>
<button class="animated flipInx" onclick="return show_div('test4')"  style="background-color:maroon;">Safe cities</button>
</td><td><button class="animated flipInx" onclick="return show_div('test5')"  style="background-color:blue;">Critical infra</button>
</td><td><button class="animated flipInx" onclick="return show_div('test6')"  style="background-color:orange;">Border security</button>	
</td></tr></table>
</div>
<div id="test2" style="display:none">
<table cellpadding="50" border=0><tr><td>
<button  class="animated zoomInLeft" onclick="return show_div('test7')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOFM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
</td><td><button  class="animated pulse" onclick="return show_div('test8')">Dial 100</button>	
</td><td><button   class="animated zoomInRight" onclick="return show_div('test9')">Bharath net</button>	
</td></tr></table>
</div>
<div id="test3" style="display:none">
	<table cellpadding="50" border=0><tr><td>
<button    class="animated slideInUp"  onclick="return show_div('test10')">Smart cities</button>
</td></tr></table>
</div>
<div id="test4" style="display:none">
<table cellpadding="50" border=0><tr><td>
<button  class="animated rollIn" onclick="return show_div('test9')"  style="background-color:maroon;">Gujarat city <br> surveillance</button>
</td><td><button   class="animated pulse" onclick="return show_div('test10')"  style="background-color:maroon;">Mumbai</button>	
</td><td><button   class="animated lightSpeedIn" onclick="return show_div('test10')"  style="background-color:maroon;">Hydrabad</button>	
</td></tr></table>
</div>
<div id="test5" style="display:none">
	<table cellpadding="50" border=0><tr><td>
<button    class="animated rubberBand"  onclick="return show_div('test9')">Sabarmati jai</button>
</td></tr></table>
</div>
<div id="test10" style="display:none">
	<table cellpadding="50" border=0><tr><td>
<button    class="animated wobble"  onclick="return show_div('test9')">Jaipur</button>
</td><td><button    class="animated wobble"  onclick="return show_div('test10')">Nagpur smart city</button>	
</td></tr></table>
</div>
<div  id="test9" style="display:none">
<?php include_once("cr.html"); ?>
</div>