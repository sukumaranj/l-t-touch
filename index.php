<?php include("db_connect.php"); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>L&T CONSTRUCTION</title>
    <meta name="description" content="">
            <style>
			table{
			border-collapse: collapse;
			border-spacing:0;
			}
			a:link, a:visited {
			color: white;
			padding: 14px 25px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			}
			a:hover, a:active {
				
			color: orange;	
			}


				
				
            </style>
        </head>
       <body style="padding:0px; margin:0px; background-color:#fff;font-family:helvetica, arial, verdana, sans-serif overflow: hidden;">
		       <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: http://www.jssor.com -->
    <!-- This code works with jquery library. -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/jssor.slider-21.1.6.mini.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,o:-1},{b:0,d:1000,o:1}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:1000,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:1900,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
        
    </script>
    <style>
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('img/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
        
        /* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        .jssora22l.jssora22lds      (disabled)
        .jssora22r.jssora22rds      (disabled)
        */
        .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('img/a22.png') center center no-repeat;
            overflow: hidden;
        }
        .jssora22l { background-position: -10px -31px; }
        .jssora22r { background-position: -70px -31px; }
        .jssora22l:hover { background-position: -130px -31px; }
        .jssora22r:hover { background-position: -190px -31px; }
        .jssora22l.jssora22ldn { background-position: -250px -31px; }
        .jssora22r.jssora22rdn { background-position: -310px -31px; }
        .jssora22l.jssora22lds { background-position: -10px -31px; opacity: .3; pointer-events: none; }
        .jssora22r.jssora22rds { background-position: -70px -31px; opacity: .3; pointer-events: none; }
    </style>
            <table width="1059" height="87" border="0">
                <tr>
                <td background="images/top_header_bg.JPEG" align="center" style="padding:0px;">
                   <h1 style="color:white;">SMART WORLD & COMMUNICATION</h1> 
                </td>
                </tr>
            </table>
            <table background="images/middle_bg.JPEG" width="1059" height="304" border="0">
                <tr align="center" style="padding:0px;">
               
                   
                   
                   
				<?php
				$cat_id	=	$_REQUEST['category_id'];
				$subcat_id	=	$_REQUEST['subcategory_id'];
				$sql_qry	=	mysql_query("select `category_name`,`id` from `tbl_category` where `status`='active'");

				while($row_cat	=	mysql_fetch_object($sql_qry)){?>

				<?php if($row_cat->id==$cat_id) {?>

				<td>	

				<div  style="border: 1px solid rgba(0, 0, 0, .2); width:220px;height:220px; background-color:#00858a;opacity: 0.6;  border-radius: 20px 25px 20px 25px;">
				<div>

				<h3><a href="index.php?category_id=<?php echo $row_cat->id;?> "><?php echo $row_cat->category_name;?></a></h3>
				</div> 
				</div>
				</td>
				<?php
				}
				else if($row_cat->id==2)
				{?>
				<td >
				<div  style="border: 1px solid rgba(0, 0, 0, .2); width:220px;height:220px; background-color:#00858a;opacity: 0.6;  border-radius: 20px 25px 20px 25px;">
				<div>

				<h3><a href="index.php?category_id=<?php echo $row_cat->id;?> "><?php echo $row_cat->category_name;?></a></h3>
				</div> 
				</div>
				</td>
				<?php
				}
				else
				{?>  <td >
				<div  style="border: 1px solid rgba(0, 0, 0, .2); width:220px;height:220px; background-color:#00858a;opacity: 0.6;  border-radius: 20px 25px 20px 25px;">
				<div>

				<h3><a href="index.php?category_id=<?php echo $row_cat->id;?> "><?php echo $row_cat->category_name;?></a></h3>
				</div> 
				</div>
				</td>
				<?php
				}
				}?>
               
               </tr>
            </table>
            
            
            
            <table width="1059" height="702" border="0">
             <tr>
				<td background="images/botttm.JPEG" align="center" style="padding:0px;">	
					
			<?php
			if(isset($cat_id))
			{
			
				
			if($subcat_id >= 1 )
			{
				$sql_qry2	=	mysql_query("select childcategory_name,id,category_id,subcategory_id from `tbl_chldcategory` where `status`='active' and  subcategory_id ='$subcat_id'");
				
				
				
				while($row_cat2	=	mysql_fetch_object($sql_qry2)){?>

				 
                    	
				<div  style="border: 1px solid rgba(0, 0, 0, .2); width:220px;height:220px; background-color:#00858a;opacity: 0.6;  border-radius: 20px 25px 20px 25px;">
				<div>

				<h3><a href="view.php?category_id=<?php echo $row_cat2->category_id;?>&subcategory_id=<?php echo $row_cat2->subcategory_id;?> &childcategory_id=<?php echo $row_cat2->id;?> " id="download-button" class="btn-large waves-effect waves-light orange"><?php echo $row_cat2->childcategory_name; ?></a></h3>
				</div> 
				</div>
				
			
			
				<?php
				}
				
				
			}
			else
			{
				if($cat_id > 1)
				{
				$sql_qry1	=	mysql_query("select subcategory_name,id,category_id from `tbl_subcategory` where `status`='active' and  category_id ='$cat_id'");

				}
				else 
				{
				$sql_qry1	=	mysql_query("select subcategory_name,id,category_id from `tbl_subcategory` where `status`='active' and category_id =1");

				}

				while($row_cat1	=	mysql_fetch_object($sql_qry1)){?>

				
				<div  style="border: 1px solid rgba(0, 0, 0, .2); width:220px;height:220px; background-color:#00858a;opacity: 0.6;  border-radius: 20px 25px 20px 25px;">
				<div>

				<h3><a href="index.php?category_id=<?php echo $row_cat1->category_id;?>&subcategory_id=<?php echo $row_cat1->id;?> " id="download-button" class="btn-large waves-effect waves-light orange"><?php echo $row_cat1->subcategory_name; ?></a></h3>
				</div> 
				</div>
				</td >
			
				<?php
				}
			
			}
			}
			else 
			{
			?>	
			
                <td align="center" style="padding:0px;">
                   <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1059px; height: 702px; overflow: hidden; visibility: hidden;">

					<div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1059px; height: 702px; overflow: hidden;">
						
					<div data-p="225.00">
					<img data-u="image" src="img/slider/1.JPEG" />
					</div>
					<div data-p="225.00">
					<img data-u="image" src="img/slider/2.JPEG" />
					</div>
					<div data-p="225.00">
					<img data-u="image" src="img/slider/3.JPEG" />
					</div>
					<div data-p="225.00">
					<img data-u="image" src="img/slider/4.JPEG" />
					</div>
					<div data-p="225.00">
					<img data-u="image" src="img/slider/5.JPEG" />
					</div>
					
					<div data-p="225.00">
					<img data-u="image" src="img/slider/6.JPEG" />
					</div>
					<div data-p="225.00">
					<img data-u="image" src="img/slider/7.JPEG" />
					</div>
					<div data-p="225.00">
					<img data-u="image" src="img/slider/8.JPEG" />
					</div>


			
			</div>

			<!-- Arrow Navigator -->
			<span data-u="arrowleft" class="jssora22l" style="top:0px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
			<span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
			</div>
                </td>
                
               <?php
			}
		    ?>
                </tr>
           
            </table>
			
    
        </body>
    </html>
